using NUnit.Framework;
using Calculadora;

namespace Calculadora.UnitTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {


        }

        [Test]
        public void Valor_QuandoAcaoForSomar_RetornaSoma()
        {
            // arrange
            var calcular = new Calcular();

            calcular.numero1 = 1;
            calcular.numero2 = 2;
            calcular.operacoes = "+";

            //act
            var resultado = calcular.Valor();

            //assert

            Assert.That(resultado, Is.EqualTo(3));
        }
        [Test]
        public void Valor_QuandoAcaoForSubtrair_RetornaSubtracao()
        {
            // arrange
            var calcular = new Calcular();

            calcular.numero1 = 1;
            calcular.numero2 = 2;
            calcular.operacoes = "-";

            //act
            var resultado = calcular.Valor();

            //assert

            Assert.That(resultado, Is.EqualTo(-1));
        }
        [Test]
        public void Valor_QuandoAcaoForMultiplicar_RetornaMultiplicacao()
        {
            // arrange
            var calcular = new Calcular();

            calcular.numero1 = 1;
            calcular.numero2 = 2;
            calcular.operacoes = "*";

            //act
            var resultado = calcular.Valor();

            //assert

            Assert.That(resultado, Is.EqualTo(2));
        }
        [Test]
        
        public void Valor_QuandoAcaoForDividir_RetornaDivisao()
        {
            // arrange
            var calcular = new Calcular();

            calcular.numero1 = 1;
            calcular.numero2 = 2;
            calcular.operacoes = "/";

            //act
            var resultado = calcular.Valor();

            //assert

            Assert.That(resultado, Is.EqualTo(0.5));
        }

    }
}
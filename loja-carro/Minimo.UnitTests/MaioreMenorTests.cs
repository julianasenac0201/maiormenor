using NUnit.Framework;
using Minimo;


namespace Minimo.UnitTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {

        }
        [Test]
        public void Valor_QuandoAcaoNum1ForMaior_RetornaNum1()
        {
            // arrange
            var maior = new Teste();

            maior.num1 = 100;
            maior.num2 = 20;
            maior.num3 = 5;

            //act
            var resultado = maior.Maior();

            //assert

            Assert.That(resultado, Is.EqualTo(100));
        }
        [Test]
        public void Valor_QuandoAcaoNum2ForMaior_RetornaNum2()
        {
            // arrange
            var maior = new Teste();

            maior.num1 = 100;
            maior.num2 = 200;
            maior.num3 = 5;

            //act
            var resultado = maior.Maior();

            //assert

            Assert.That(resultado, Is.EqualTo(200));
        }
        [Test]
        public void Valor_QuandoAcaoNum3ForMaior_RetornaNum3()
        {
            // arrange
            var maior = new Teste();

            maior.num1 = 100;
            maior.num2 = 200;
            maior.num3 = 300;

            //act
            var resultado = maior.Maior();

            //assert

            Assert.That(resultado, Is.EqualTo(300));
        }
        public void Valor_QuandoAcaoNum1ForMenor_RetornaNum1()
        {
            // arrange
            var maior = new Teste();

            maior.num1 = 2;
            maior.num2 = 20;
            maior.num3 = 5;

            //act
            var resultado = maior.Menor();

            //assert

            Assert.That(resultado, Is.EqualTo(2));
        }
        [Test]
        public void Valor_QuandoAcaoNum2ForMenor_RetornaNum2()
        {
            // arrange
            var maior = new Teste();

            maior.num1 = 100;
            maior.num2 = 1;
            maior.num3 = 5;

            //act
            var resultado = maior.Menor();

            //assert

            Assert.That(resultado, Is.EqualTo(1));
        }
        [Test]
        public void Valor_QuandoAcaoNum3ForMenor_RetornaNum3()
        {
            // arrange
            var maior = new Teste();

            maior.num1 = 100;
            maior.num2 = 200;
            maior.num3 = 10;

            //act
            var resultado = maior.Menor();

            //assert

            Assert.That(resultado, Is.EqualTo(10));
        }
    }
}